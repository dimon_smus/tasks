
string = File.open("input.txt", "r").gets.split ' '

number, total = string[0].to_i, string[1].to_i
aim = Array.new(number, 6)

def getTotal(aim)
  total = 0

  aim.each do |item|
    total+= item
  end

  return total
end

def arrayMagic(aim)

  aim[aim.length - 1] -= 1

  if aim[aim.length - 1] < 0
    aim.each_with_index do |item, index|
      if aim[aim.length - index - 1] <= 0
        aim[aim.length - index - 1] = 6
      else
        aim[aim.length - index - 1] -= 1
        break
      end
    end
  end
end

endCalc, increase = false, 0.0

if number * 6 >= total

  while !endCalc
    if getTotal(aim) == 0
      endCalc = !endCalc
    else if getTotal(aim) == total
           increase += 1
         end
    end

    arrayMagic(aim)
  end

end

puts increase/(6.0**number)