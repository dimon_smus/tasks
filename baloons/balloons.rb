#Define variable we want to use
baloonsArray = []
startLine = endLine = nil


#Process file to put all strings to array
File.open("input.txt", "r") do |infile|
  while (line = infile.gets)
    baloonsArray.push(line)
  end
end

baloonsArray.each_with_index do |string, index|
  if string.include? 'X'
    endLine = index
  else if string.include? '@'
         startLine = index
       end
  end
end

def leftDir(line)

end

#Enter here only if there is correct start and end positions
if startLine && endLine
  baloonsArray.map! do |string|
    string.split(//)
  end

  startLine = [startLine, endLine].min
  endLine = [startLine, endLine].max

end

print baloonsArray, endLine, startLine