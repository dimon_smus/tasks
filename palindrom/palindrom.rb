
def replaceSymbol(left, right)
  swapped = false

  (0..left.length).each do |i|
    if left[i] != right[i]
      swapped = !swapped
    else if left[i] != right[i] && swapped

           return false
         end
    end
  end

  return true
end

def deleteSymbol(left, right)
  deleted = false

  #right part is smaller
  (0..right.length).each do |i|

    if left[i] != right[i]

      left = left[0..i - 1] + left[i + 1..-1]

      deleted = !deleted
    else if left[i] != right[i] && deleted

           return false
         end
    end
  end

  return true
end

def diffEqualLengthParts(left, right)

  if left == right
    return true
  else
    return replaceSymbol(left, right)
  end
end

def diffDifferentLengthParts(left, right)

  if left[0..left.length - 1] == right
    return true
  else
    return deleteSymbol(left, right)
  end
end

def diff(string)
  if string.length % 2 == 0
    return diffEqualLengthParts(string[0..string.length / 2 - 1], string[string.length / 2..-1].reverse!)
  else
    return diffDifferentLengthParts(string[0..string.length / 2], string[string.length / 2 + 1..-1].reverse!)
  end
end

#Process file to put strings to array
string = File.open("input.txt", "r").gets

string = (string.delete ' ').delete ','

puts string.downcase!

puts diff(string)