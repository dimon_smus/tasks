
string = File.open("input.txt", "r").gets.split('/')

topNumber, bottomNumber = string[0], string[1]

dictionary = {
    'I' => 1,
    'V' => 5,
    'X' => 10,
    'L' => 50,
    'C' => 100,
    'D' => 500,
    'M' => 1000
}

def getNumber(string, dictionary)
  total = 0
  
  string.split(//).each_with_index do |item, index|

    total += dictionary[item]

    if dictionary[string[index + 1]] && dictionary[item] < dictionary[string[index + 1]]
      total = -total
    end
  end

  return total
end

puts getNumber(topNumber, dictionary) / getNumber(bottomNumber, dictionary).to_f


