

number = File.open("input.txt", "r").gets.to_i

aim = [1]

def getPositionsNumber(number, aim)

  (1..number).each_with_index do |item, index|
    aim.push((4*number - 2)/(number + 1) * aim[index - 1])
  end

  return aim[aim.length - 1]
end

puts getPositionsNumber(number / 2, aim)

number = 17

base = Math.sqrt(number).round

puts base

def factorial(number)
  value = 1

  (1..number).each do |item|
    value*=item
  end

  return value
end

def helper(number)
  value = 1.0

  (1..number).each do |item|
    if item%2 == 0
      value += 1.0/factorial(item)
    else
      value -= 1.0/factorial(item)
    end
  end

  return value
end

puts factorial(5)*helper(5)